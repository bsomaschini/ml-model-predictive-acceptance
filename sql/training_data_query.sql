SELECT pa.person_id, pa.scheduled_event_id, pa.scheduled_store_id, pa.inventory_dt,
pa.is_backroom_flag, pa.is_msr_flag, pa.is_night_shift_flag, pa.is_weekend_flag, pa.is_rx_event_flag, 
pa.distance_person_store_miles,
atc.person_gender_cd,
(DATE_PART('year', pa.inventory_dt)-DATE_PART('year', atc.person_doh_dt))as tenure,
(DATE_PART('year', pa.inventory_dt)-DATE_PART('year', atc.person_dob_dt))as "age",
coalesce(atc.job, 'unknown') as job,
atc.person_inventory_total_hrs, 
atc.per_aph_avg,
atc.ml_aset_type, atc.shift_preference,
pa.change_request_type_code,
pa.no_show
from aiaes_data.ml_ipt_predictive_acceptance pa
left join(SELECT ac.person_id,
ac.person_dob_dt, 
ac.person_current_job_desc,
ac.person_gender_cd,
ac.person_doh_dt::timestamp::date,
(case when ac.person_current_job_desc like '%Manager%' then 'manager' when  ac.person_current_job_desc like '%Auditor%' then 'auditor' 
when ac.person_current_job_desc like '%Specialist%' then 'specialist' when ac.person_current_job_desc like '%Expert%' then 'expert'
when ac.person_current_job_desc like '%Top Gun%' then 'topgun' when ac.person_current_job_desc like '%Associate%' then 'associate'
when ac.person_current_job_desc like '%TmLdr%' then 'tmldr' when ac.person_current_job_desc like '%Supervisor%' then 'supervisor' 
 else 'other' end) as job,
ac.person_inventory_total_hrs, 
ac.ml_aset_type, ac.shift_preference, ac.person_mean_aph as per_aph_avg
from aiaes_data.ml_ipt_aset_classification ac
) atc on atc.person_id=pa.person_id
