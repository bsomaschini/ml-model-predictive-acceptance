select '{sol_id}' as solution_id, 
 dt1.person_id,  dt1.scheduled_event_id,  dt1.is_backroom_flag,  dt1.is_msr_flag,  dt1.is_night_shift_flag,
  dt1.is_weekend_flag,  dt1.is_rx_event_flag,
  (select avg(distance_person_store_miles) as distance_person_store_miles  
	from aiaes_data.ml_ipt_predictive_acceptance  where 
	(case when (store_zip_code=dt1.store_zip and person_zip_code=dt1.person_zip) or 
	(store_zip_code=dt1.person_zip and person_zip_code=dt1.store_zip) then 1 else 0 end)=1) as distance_person_store_miles, 
(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_doh_dt))as tenure,
(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_dob_dt))as "age",  
dt1.person_gender_cd,  
dt1.job,  
dt1.person_inventory_total_hrs,  
dt1.person_mean_aph,  
dt1.ml_aset_type,  
dt1.shift_preference
from (
  	(SELECT event_id as scheduled_event_id,
  	store_zip,  	
	inv_date,
  	msr_flag as is_msr_flag,
  	(case when br_estimates>0 then 1 else 0 end) as is_backroom_flag,
  	(case when am_pm='PM' then 1 else 0 end) as is_night_shift_flag,
  	rx_need as is_rx_event_flag,
  	(case when (extract(dow from inv_date::timestamp)=6 or extract(dow from inv_date::timestamp)=0) then 1 else 0 end) as is_weekend_flag
  	from aiaes_data.aes_stores
  	where event_id={event_id}) event_fields
  cross join (
  	SELECT ac.person_id,  	
	ac.person_dob_dt,
   	ac.person_current_job_desc,
  	ac.person_gender_cd,
  	ac.person_doh_dt::timestamp::date,
  	(case when ac.person_current_job_desc like '%Manager%' then 'manager' when  ac.person_current_job_desc like '%Auditor%' then 'auditor'
   	when ac.person_current_job_desc like '%Specialist%' then 'specialist' when ac.person_current_job_desc like '%Expert%' then 'expert'
  	when ac.person_current_job_desc like '%Top Gun%' then 'topgun' when ac.person_current_job_desc like '%Associate%' then 'associate'
  	when ac.person_current_job_desc like '%TmLdr%' then 'tmldr' when ac.person_current_job_desc like '%Supervisor%' then 'supervisor'
   	else 'other' end) as job,
  	ac.person_inventory_total_hrs,
   	ac.person_mean_aph,
  	ac.ml_aset_type,
	ac.shift_preference,
	ac.person_zip_code as person_zip
  	from aiaes_data.ml_ipt_aset_classification ac
  	where person_id in ({str_all_people}))  person_fields) dt1;
						