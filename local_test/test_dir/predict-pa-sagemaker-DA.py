import boto3
import argparse
import sys
import os
import io
import pandas as pd
import itertools
import json

from time import time

# Set below parameters
bucket = 'rgis-sagemaker-data'
key = 'rgis-predictive-acceptance-estimator/input/data/training/train'
endpointName = 'endpoint-pred-acc-estimator-prod'

parser = argparse.ArgumentParser(description='Run endpoint in Data Access mode (CI or SEA).')
parser.add_argument('ci', 
                    metavar='CI',
                    type=str,
                    help='CI(Bool): whether to run in CI or SEA mode.')

args = parser.parse_args()

ci  = eval(args.ci)

print(f"Endpoint: {endpointName}")
start_time = time()

data_ci = {'solution_ids': [0, 1, 2, 0, 0, 0, 0, 0, 0], 'sched_event_ids': [25526308, 25526308, 25526308, 25526307, 25526306, 25501660, 25500495, 25248628, 25044557], 'aes_teams': [[20509, 20820, 21069], [25763, 20820, 21069], [25763, 26102, 21069], [26209], [11981], [12037], [15431, 20509], [26211], [26211]]}
data_sea = {
    "sched_event_id": "26369219",
    "aset_avail_list": [1361616,1440620,1482936,8182,545995,647339,720580,1226973,1319384,1324327,1353205]
  }


data_dump = json.dumps(data_ci if ci else data_sea)
print(endpointName)

# Talk to SageMaker
client = boto3.client('sagemaker-runtime')
response = client.invoke_endpoint(
    EndpointName=endpointName,
    CustomAttributes=f"is_complex_index:{ci}",
    Body=data_dump,
    ContentType='application/json',
    Accept='Accept'
)

print((time() - start_time)*1000)
response_stream = response['Body'].read().decode('ascii')

json_results = json.loads(response_stream)

# df = pd.DataFrame(json_results)
print(json_results)


