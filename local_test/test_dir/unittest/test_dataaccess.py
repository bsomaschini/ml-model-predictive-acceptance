from rgis.ml.predictive_acceptance.dataaccess import PredictiveAcceptanceInference

from unittest import TestCase
import os
from time import time


class TestDataAccess(TestCase):
	def setUp(self):
		pass

	def test_set_redshift_con(self):
		redshift_obj = PredictiveAcceptanceInference()
		self.assertTrue(redshift_obj)

		redshift_obj.set_redshift_con()
		self.assertIsNotNone(redshift_obj.redshift)


	def test_get_inference_data(self):
		start_time = time()
		sched_event_id = 24960186
		aset_avail_list = [1376054, 1496768, 1478497, 955163, 1497565, 1498131, 1061058, 1496112, 1425385, 1437615,
		                   1465234, 1361537, 9159, 1484351, 13305, 1494750, 1207714, 1496765, 1494775, 1279343, 1495411,
		                   1481460, 1417746, 1440360, 1490576, 1467772, 1422345, 394463, 13863, 1493219, 1474502, 1421270,
		                   1457184, 527320, 1425292, 485923, 1369999, 422500, 1493695, 1476004, 1186311, 1263172, 1237984,
		                   1474505, 1485071, 1466585, 647616, 1481297, 1492329, 1493221, 1493704, 13667, 1276793, 1462935,
		                   1492354]

		redshift_obj = PredictiveAcceptanceInference()
		self.assertTrue(redshift_obj)

		redshift_obj.set_redshift_con()
		self.assertIsNotNone(redshift_obj.redshift)

		df = redshift_obj.get_inference_data(None, sched_event_id, aset_avail_list)
		print(f"time taken {time() - start_time}")
		print(df.head())
		self.assertTrue(df.shape[0] > 0)

	def test_get_inference_data_ci(self):
		start_time = time()
		solution_id = [0, 0]
		sched_event_id = [24034296, 24034302]
		aset_avail_list = [
			[1236560, 1341477, 1239963, 231723, 193629, 1030051, 1392386],
			[193629, 1239963, 969386, 1392386, 1236560, 231723, 1341477]
		]

		redshift_obj = PredictiveAcceptanceInference()
		self.assertTrue(redshift_obj)

		redshift_obj.set_redshift_con()
		self.assertIsNotNone(redshift_obj.redshift)

		df = redshift_obj.get_inference_data(solution_id, sched_event_id, aset_avail_list, is_complex_index=True)
		print(f"time taken {time() - start_time}")
		print(df.head())
		self.assertTrue(df.shape[0] > 0)
