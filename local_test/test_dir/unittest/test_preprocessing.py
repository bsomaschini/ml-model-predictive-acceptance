from unittest import TestCase
from os import path, getcwd

import pandas
from numpy import nan
from collections import Counter

from rgis.ml.predictive_acceptance.preprocessing import FillNaValues, PickContextEvents, AggregatePersonData, \
	ReplaceJobTitle, ApplyNoShowFlags, BooleanEncoding, SMOTEOverSampling


class TestPreProcessing(TestCase):

	def setUp(self):
		self.data_file = path.normpath(path.join(getcwd(), "../input/data/training/train"))
		self.input_df = pandas.read_csv(self.data_file)


	# def test_to_datetime_default(self):
	# 	columns = ["IN_TIME", "OUT_TIME"]
	# 	converter = ConvertToDateTime(columns=columns)
	# 	converter.fit(self.input_df)
	#
	# 	output = converter.transform(self.input_df)
	# 	for col in columns:
	# 		self.assertTrue(is_datetime64_dtype(output[col]))
	#
	# def test_to_datetime_with_format(self):
	# 	columns = ["IN_TIME", "OUT_TIME"]
	# 	datetime_format = "%Y-%m-%d %H:%M"
	# 	converter = ConvertToDateTime(columns=columns, datetime_format=datetime_format)
	# 	converter.fit(self.input_df)
	#
	# 	output = converter.transform(self.input_df)
	# 	for col in columns:
	# 		self.assertTrue(is_datetime64_dtype(output[col]))
	#
	# def test_remove_nan(self):
	# 	columns = ["IN_TIME", "OUT_TIME"]
	# 	nan_remover = RemoveNanValues(columns=columns)
	# 	nan_remover.fit(self.input_df)
	#
	# 	output = nan_remover.transform(self.input_df)
	# 	self.assertTrue(output.shape[0] < self.input_df.shape[0])

	def test_fill_nan(self):
		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN"}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)

		filled_df = output[output["PERSON_CURRENT_JOB_DESC"] == columns["PERSON_CURRENT_JOB_DESC"]]
		self.assertTrue(filled_df.shape[0] > 0)

	def test_apply_no_show_flags(self):
		apply_no_show_flag = ApplyNoShowFlags()
		apply_no_show_flag.fit(self.input_df)

		output = apply_no_show_flag.transform(self.input_df)

		expected = set([nan, 'RESCHDULED', 'EVENT_CANNOT_ATTEND', 'EVENT_LEAVE_EARLY', 'EVENT_ATTEND_LATE', 'NO_SHOW'])
		self.assertTrue(set(output['CHANGE_REQUEST_TYPE_CODE'].unique()) == expected)

	def test_replace_job_title(self):
		columns = ["PERSON_CURRENT_JOB_DESC"]
		replace_job_title = ReplaceJobTitle(replace_columns=columns)
		replace_job_title.fit(self.input_df)

		output = replace_job_title.transform(self.input_df)

		self.assertTrue(len(output['PERSON_CURRENT_JOB_DESC'].unique()) < len(self.input_df['PERSON_CURRENT_JOB_DESC'].unique()))

	def test_pick_latest_context(self):
		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN"}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)
		transformer = PickContextEvents(context_length=5, for_training=True)
		output = transformer.transform(output)
		for idx, group in output.groupby(['PERSON_ID']):
			self.assertTrue(group.shape[0] <= 5)

	def test_pick_latest_context_for_training_false(self):
		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN"}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)
		transformer = PickContextEvents(context_length=5, for_training=False)
		output = transformer.transform(output)
		for idx, group in output.groupby(['PERSON_ID']):
			self.assertTrue(group.shape[0] <= 5)

	def test_aggregate_person_data(self):
		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN"}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)
		transformer = PickContextEvents(context_length=5, for_training=False)
		output = transformer.transform(output)

		aggregator = AggregatePersonData(for_training=True)
		aggragated = aggregator.transform(output)

		self.assertEqual(aggragated.shape[0], len(output['PERSON_ID'].unique()))

	def test_boolean_encoder(self):

		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN"}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)

		transformer = PickContextEvents(context_length=5, for_training=True)
		output = transformer.transform(output)

		aggregator = AggregatePersonData(for_training=True)
		aggragated = aggregator.transform(output)

		lambda_func = lambda x: True if x != 'NO_CHANGE' else False
		boolean_encoder = BooleanEncoding("CHANGE_REQUEST_TYPE_CODE", "IS_CR", lambda_func)
		output = boolean_encoder.transform(aggragated)

		self.assertEqual(set(output['IS_CR'].unique()), set([True, False]))

	def test_over_sampling(self):
		columns = {"PERSON_CURRENT_JOB_DESC": "UNKNOWN", "HOURS_TOTAL": 0, "PER_APH_AVG": 0, "NUMBER_OF_BREAKS_CNT": 0,
		           "BREAK_VIOLATION_CNT": 0, "MEAL_VIOLATION_CNT": 0}
		fill_nan = FillNaValues(fill_values=columns)
		fill_nan.fit(self.input_df)

		output = fill_nan.transform(self.input_df)

		transformer = PickContextEvents(context_length=5, for_training=True)
		output = transformer.transform(output)

		aggregator = AggregatePersonData(for_training=True)
		aggragated = aggregator.transform(output)

		features = ['AVG_HOURS', 'AVG_NUMBER_OF_BREAKS', 'AVG_PER_APH', 'PERSON_CURRENT_JOB_DESC',
		            'PCT_BACKROOM_FLAG', 'PCT_MSR_FLAG', 'PCT_NIGHT_SHIFT', 'PERSON_ID']

		target = 'CHANGE_REQUEST_TYPE_CODE'
		label_columns = ['PERSON_CURRENT_JOB_DESC']

		print(Counter(aggragated['CHANGE_REQUEST_TYPE_CODE']))
		oversampling = SMOTEOverSampling(features, target, label_columns)
		df = oversampling.transform(aggragated)

		counts = Counter(df[target])
		for k, v in counts.items():
			self.assertEqual(v, 1679)
