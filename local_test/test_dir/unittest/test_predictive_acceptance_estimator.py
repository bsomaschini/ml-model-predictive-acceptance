from unittest import TestCase
import pandas as pd
from os import path, getcwd
from sklearn.metrics import f1_score
import os
import json
import random
import numpy as np
from sklearn.preprocessing import OneHotEncoder
import yaml
from rgis.ml.predictive_acceptance import PredictiveAcceptanceEstimator

if __name__ == '__main__': 
    df=pd.read_csv("./input/data/train2.csv")
    df=df[0:1000]
    df_test=pd.read_csv("./input/data/test2.csv")

    subset_test=df_test.loc[df_test["scheduled_event_id"].isin([26348894,26196084])]
    df_test=df[0:1000]
    filename="./././input/config/hyperparameters.json"
    # read file
    with open(filename, 'r') as f:
        parametri_modello = json.load(f)

    cv=parametri_modello["categorical_columns"].split(',')
    mific=yaml.load(parametri_modello["fix_mi_variable"])
    print(parametri_modello)
    pa=PredictiveAcceptanceEstimator(**parametri_modello)
    pa.fit(df)
    res=pa.predict(df_test)
    print(res)

    #switch complex index to true
    parametri_modello['is_complex_index']=True
    print(subset_test.shape)
    subset_test["solution_id"]=np.random.randint(2, size=subset_test.shape[0])
    res=pa.predict(subset_test)
    print(res)





