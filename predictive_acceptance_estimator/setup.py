import setuptools
import os

base_dir = os.path.dirname(__file__)

with open(os.path.join(base_dir, "README.md"), "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='rgis',
    version='0.2',
    author="Beatrice Somaschini",
    author_email="beatrice.somaschini@blackstraw.ai",
    description="Python module contains dependencies of predictive acceptance estimator",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/bsomaschini/ml-model-predictive-acceptance/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Owned and managed by RGIS LLC.",
        "Operating System :: OS Independent",
    ],
    install_requires=['numpy==1.16.2','scipy==1.2.1','scikit-learn==0.20.2','pandas==0.24.0',
                     'flask','gevent','gunicorn','xgboost==0.82','imbalanced-learn==0.4.3',
                     'dill==0.2.9','pyYaml','seaborn','boto3', 'os', 'copy', 'datetime', 'math', 'psycopg2', 'joblib==0.13.2'],
    package_data = {
         '': ['*.pkl'],
    }
)
