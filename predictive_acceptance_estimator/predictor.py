# This is the file that implements a flask server to do inferences. It's the file that you will modify to
# implement the scoring for your own algorithm.

from __future__ import print_function

import os
import dill
import io
import json

import flask

import pandas
import numpy

from rgis.ml.predictive_acceptance.dataaccess import PredictiveAcceptanceInference
from rgis.ml.predictive_acceptance import model_file_name

prefix = '/opt/ml/'
model_path = os.path.join(prefix, 'model')


# A singleton for holding the model. This simply loads the model and holds it.
# It has a predict function that does a prediction based on the model and the input data.
class ScoringService(object):
    model = None                # Where we keep the model when it's loaded
    redshift = None
    try:
        redshift = PredictiveAcceptanceInference()
        redshift.set_redshift_con()
    except Exception as ex:
        print("Failed to initiate connection with redshift due to: " + str(ex))

    @classmethod
    def get_inference_data(cls, data, is_complex_index):
        try:
            if is_complex_index:
                solution_ids = data['solution_ids']
                sched_event_ids = data['sched_event_ids']
                aset_avail_lists = data['aes_teams']

                return cls.redshift.get_inference_data(solution_ids, sched_event_ids, aset_avail_lists, is_complex_index)
            else:
                sched_event_id = data['sched_event_id']
                aset_avail_list = data['aset_avail_list']

                return cls.redshift.get_inference_data(None, sched_event_id, aset_avail_list, is_complex_index)
        except KeyError as ke:
            print(f"KEY NOT FOUND: {ke}")
            return ke

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.model is None:
            with open(os.path.join(model_path, 'rgis-predictive-acceptance-estimator.pkl'), 'rb') as inp:
                cls.model = dill.load(inp)
        return cls.model

    @classmethod
    def predict(cls, data, is_complex_index=False):
        """For the input, do the predictions and return them.

        Args:
            data (a pandas dataframe): The data on which to do the predictions. There will be
                one prediction per row in the dataframe
            is_complex_index: calculation will be done specially for complex index
        """
        clf = cls.get_model()
        predictions=clf.predict(X=data, is_complex_index=is_complex_index)
        print(predictions)
        if is_complex_index:
            events=[]
            solution_id=[]
            acceptance_score=[]
            team=[]
            for k1,item1 in predictions.items():
                for k2, item2 in item1.items():
                    events.append(k1)
                    solution_id.append(k2)
                    acceptance_score.append(item2["acceptance_score"])
                    team.append(item2["aes_team"])
            predictions=pandas.DataFrame({'solution_id':solution_id,'scheduled_event_id':events, 'acceptance_score':acceptance_score,'person_id_list':team}).to_dict()
        else:
            predictions=pandas.DataFrame({'person_id':list(list(predictions.values())[0].keys()),'acceptance_score':list(list(predictions.values())[0].values())}).to_dict()
        print(predictions)
        print("step after the predict")
        return predictions





# The flask app for serving predictions
app = flask.Flask(__name__)


@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy. In this sample container, we declare
    it healthy if we can load the model successfully."""
    health = ScoringService.get_model() is not None  # You can insert a health check here

    status = 200 if health else 404
    return flask.Response(response='\n', status=status, mimetype='application/json')


@app.route('/invocations', methods=['POST'])
def transformation():
    """Do an inference on a single batch of data. In this sample server, we take data as CSV, convert
    it to a pandas data frame for internal use and then convert the predictions back to CSV (which really
    just means one prediction per line, since there's a single column.
    """
    try:
        # check if the request is for complex index
        is_complex_index = check_is_complex_index(flask.request)

        # Convert from CSV to pandas
        if flask.request.content_type == 'text/csv':
            data = flask.request.data.decode('utf-8')
            s = io.StringIO(data)
            data = pandas.read_csv(s)
        elif flask.request.content_type == 'application/json':
            print("application/json type recieved")
            data = flask.request.json
            print(f"Request input: {data}")
            data = ScoringService.get_inference_data(data, is_complex_index)
   
        else:
            return flask.Response(response='This predictor only supports CSV or JSON data', status=415,
                                  mimetype='text/plain')

        print('Invoked with {} records'.format(data.shape[0]))

        try:
            print("make the prediction")
            # Do the prediction
            predictions = ScoringService.predict(data=data, is_complex_index=is_complex_index)
            print("Results of the predict function:")
            print(predictions)
            results = pandas.DataFrame(predictions).to_json()

        except Exception as ex:
            results = json.dumps({'status': 'error', 'error_message': str(ex)})

        print(results)
        return flask.Response(response=results, status=200, mimetype='application/json')
    except Exception as ex:
        print(str(ex))
        error = {"status": "ERROR", "error_message": str(ex)}
        return error, 401


def default(o):
    if isinstance(o, numpy.int64):
        return int(o)
    elif isinstance(o, numpy.float64):
        return float(o)
    else:
        return o


def check_is_complex_index(request_data):
    try:
        custom_attr = request_data.headers.get('X-Amzn-SageMaker-Custom-Attributes')
        print(f"Given Custom Attribute value {custom_attr}")
        if custom_attr:
            return custom_attr.split(':')[1].lower() == 'true'
        else:
            return False
    except Exception as ex:
        print(str(ex))
        print("Consider this has a not a complex index request")
        return False
