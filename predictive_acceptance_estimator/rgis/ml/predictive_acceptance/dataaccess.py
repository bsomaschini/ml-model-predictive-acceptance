import os
import boto3
import sys
import psycopg2
import pandas


class PredictiveAcceptanceInference:
	def __init__(self):
		self.redshift = None
		self.rs_env = os.environ.get('REDSHIFT_INSTANCE')
		self.rs_env = self.rs_env.upper() if self.rs_env else None

	def set_redshift_con(self):
		print('Getting parameters for Redshift connection...')
		ssmParams = False
		if (os.environ.get('DB_NAME') == None
				or os.environ.get('DB_HOST') == None
				or os.environ.get('DB_PORT') == None
				or os.environ.get('DB_USER') == None
				or os.environ.get('DB_PWD') == None):
			print('Use SSM parameters.')
			ssmParams = True

		if ssmParams:
			try:
				ssm = boto3.client('ssm')
			except:
				raise RuntimeError('Unable to create SSM client - ' + str(sys.exc_info()))
			try:
				if self.rs_env == 'DEV':
					os.environ['DB_NAME'] = \
					ssm.get_parameter(Name='/DEV/Redshift/aiaes-db-name', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_HOST'] = \
					ssm.get_parameter(Name='/DEV/Redshift/aiaes-db-host', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_PORT'] = \
					ssm.get_parameter(Name='/DEV/Redshift/aiaes-db-port', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_USER'] = \
					ssm.get_parameter(Name='/DEV/Redshift/aiaes-db-user', WithDecryption=True)["Parameter"]["Value"]
					os.environ['DB_PWD'] = \
					ssm.get_parameter(Name='/DEV/Redshift/aiaes-db-pwd', WithDecryption=True)["Parameter"]["Value"]
				elif self.rs_env == 'QA':
					os.environ['DB_NAME'] = \
					ssm.get_parameter(Name='/QA/Redshift/aiaes-db-name', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_HOST'] = \
					ssm.get_parameter(Name='/QA/Redshift/aiaes-db-host', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_PORT'] = \
					ssm.get_parameter(Name='/QA/Redshift/aiaes-db-port', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_USER'] = \
					ssm.get_parameter(Name='/QA/Redshift/aiaes-db-user', WithDecryption=True)["Parameter"]["Value"]
					os.environ['DB_PWD'] = \
					ssm.get_parameter(Name='/QA/Redshift/aiaes-db-pwd', WithDecryption=True)["Parameter"]["Value"]
				elif self.rs_env == 'PROD':
					os.environ['DB_NAME'] = \
					ssm.get_parameter(Name='/PROD/Redshift/aiaes-db-name', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_HOST'] = \
					ssm.get_parameter(Name='/PROD/Redshift/aiaes-db-host', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_PORT'] = \
					ssm.get_parameter(Name='/PROD/Redshift/aiaes-db-port', WithDecryption=False)["Parameter"]["Value"]
					os.environ['DB_USER'] = \
					ssm.get_parameter(Name='/PROD/Redshift/aiaes-db-user', WithDecryption=True)["Parameter"]["Value"]
					os.environ['DB_PWD'] = \
					ssm.get_parameter(Name='/PROD/Redshift/aiaes-db-pwd', WithDecryption=True)["Parameter"]["Value"]
				else:
					raise RuntimeError('Invalid DATABASE ENVIRONMENT value "' + self.rs_env + '"')
			except Exception as ex:
				print(str(ex))
				raise RuntimeError(str(ex))

		else:
			print('Use environment parameters.')

		# create redshift object
		self.redshift = RedshiftDBMS()

	@staticmethod
	def validate_for_ci(solution_id, scheduled_event_id, aset_avail_list):
		if not solution_id or type(solution_id) != list:
			raise ArithmeticError('For complex index, list of solution id is required')
		if not scheduled_event_id or type(scheduled_event_id) != list:
			raise ArithmeticError('For complex index, list of scheduled event id is required')
		if not aset_avail_list or type(aset_avail_list) != list:
			raise ArithmeticError('For complex index, list of aset_avail_person per event is required')
		if len(solution_id) != len(scheduled_event_id):
			raise ArithmeticError('For complex index, length of solution id should match with len of sched_event_id list')
		if len(scheduled_event_id) != len(aset_avail_list):
			raise ArithmeticError('For complex index, length of sched_event_id should match with len of aset_avail_list list')

	def get_inference_data(self, solution_id, scheduled_event_id, aset_avail_list, is_complex_index=False):
		# Build SQL Query to fetch inference data
		if is_complex_index:
			PredictiveAcceptanceInference.validate_for_ci(solution_id, scheduled_event_id, aset_avail_list)
			df_collection = []
			missing_events = []
			for sol_id, event_id, people in zip(solution_id, scheduled_event_id, aset_avail_list):
				str_all_people = ",".join(map(str, people))
				try:
					query = f"""
							select '{sol_id}' as solution_id,
								dt1.person_id,
								dt1.scheduled_event_id,
								dt1.is_backroom_flag,
								dt1.is_msr_flag,
								dt1.is_night_shift_flag,
								dt1.is_weekend_flag,
								dt1.is_rx_event_flag,
								(select avg(distance_person_store_miles) as distance_person_store_miles
								from aiaes_data.ml_ipt_predictive_acceptance
								where (case when (store_zip_code=dt1.store_zip and person_zip_code=dt1.person_zip) or (store_zip_code=dt1.person_zip and person_zip_code=dt1.store_zip) then 1 else 0 end)=1) as distance_person_store_miles,
								(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_doh_dt))as tenure,
								(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_dob_dt))as "age",
								dt1.inv_date,
								dt1.job,
								dt1.person_inventory_total_hrs,
								dt1.person_mean_aph as per_aph_avg,
								dt1.ml_aset_type,
								dt1.shift_preference
							from (
									(SELECT event_id as scheduled_event_id,
									store_zip,
									inv_date,
									msr_flag as is_msr_flag,
									(case when br_estimates>0 then 1 else 0 end) as is_backroom_flag,
									(case when am_pm='PM' then 1 else 0 end) as is_night_shift_flag,
									rx_need as is_rx_event_flag,
									(case when (extract(dow from inv_date::timestamp)=6 or extract(dow from inv_date::timestamp)=0) then 1 else 0 end) as is_weekend_flag
									from aiaes_data.aes_stores
									where event_id={event_id}) event_fields
								cross join (
									SELECT ac.person_id,
									ac.person_dob_dt, 
									ac.person_current_job_desc,
									ac.person_gender_cd,
									ac.person_doh_dt::timestamp::date,
									(case when ac.person_current_job_desc like '%Manager%' then 'manager' when  ac.person_current_job_desc like '%Auditor%' then 'auditor' 
									when ac.person_current_job_desc like '%Specialist%' then 'specialist' when ac.person_current_job_desc like '%Expert%' then 'expert'
									when ac.person_current_job_desc like '%Top Gun%' then 'topgun' when ac.person_current_job_desc like '%Associate%' then 'associate'
									when ac.person_current_job_desc like '%TmLdr%' then 'tmldr' when ac.person_current_job_desc like '%Supervisor%' then 'supervisor' 
									else 'other' end) as job,
									ac.person_inventory_total_hrs, 
									ac.person_mean_aph,
									ac.ml_aset_type, ac.shift_preference, ac.person_zip_code as person_zip
									from aiaes_data.ml_ipt_aset_classification ac
									where person_id in ({str_all_people}))  person_fields) dt1;
							"""

					df_subset = self.redshift.get_sql_dataframe(query)

					# Fill event, store data with single value
					if df_subset['scheduled_event_id'].isna().sum() == df_subset.shape[0]:
						raise ValueError(f"scheduled event id {event_id} is not found")

					for fill_c in ['scheduled_event_id', 'inv_date']:
						mode_ = df_subset[fill_c].mode()
						if len(mode_) > 0:
							df_subset[fill_c].fillna(mode_.iloc[0], inplace=True)
						else:
							df_subset[fill_c].fillna(0, inplace=True)

					df_collection.append(df_subset)

				except ValueError as ve:
					err = str(ve)
					missing_events.append(event_id)
					print(str(err))

			if df_collection:
				output = pandas.concat(df_collection).reset_index()
				output=output.drop(["index"],axis=1)
				return output
			else:
				return pandas.DataFrame()
		
		else:
			str_aset_avail = ",".join(map(str, aset_avail_list))

			query = f"""
					select dt1.person_id,
						dt1.scheduled_event_id,
						dt1.is_backroom_flag,
						dt1.is_msr_flag,
						dt1.is_night_shift_flag,
						dt1.is_weekend_flag,
						dt1.is_rx_event_flag,
						(select avg(distance_person_store_miles) as distance_person_store_miles
						from aiaes_data.ml_ipt_predictive_acceptance
						where (case when (store_zip_code=dt1.store_zip and person_zip_code=dt1.person_zip) or (store_zip_code=dt1.person_zip and person_zip_code=dt1.store_zip) then 1 else 0 end)=1) as distance_person_store_miles,
						(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_doh_dt))as tenure,
						(DATE_PART('year', dt1.inv_date)-DATE_PART('year', dt1.person_dob_dt))as "age",
						dt1.inv_date,
						dt1.job,
						dt1.person_inventory_total_hrs,
						dt1.person_mean_aph as per_aph_avg,
						dt1.ml_aset_type,
						dt1.shift_preference
					from (
							(SELECT event_id as scheduled_event_id,
							store_zip,
							inv_date,
							msr_flag as is_msr_flag,
							(case when br_estimates>0 then 1 else 0 end) as is_backroom_flag,
							(case when am_pm='PM' then 1 else 0 end) as is_night_shift_flag,
							rx_need as is_rx_event_flag,
							(case when (extract(dow from inv_date::timestamp)=6 or extract(dow from inv_date::timestamp)=0) then 1 else 0 end) as is_weekend_flag
							from aiaes_data.aes_stores
							where event_id={scheduled_event_id}) event_fields
						cross join (
							SELECT ac.person_id,
							ac.person_dob_dt, 
							ac.person_current_job_desc,
							ac.person_gender_cd,
							ac.person_doh_dt::timestamp::date,
							(case when ac.person_current_job_desc like '%Manager%' then 'manager' when  ac.person_current_job_desc like '%Auditor%' then 'auditor' 
							when ac.person_current_job_desc like '%Specialist%' then 'specialist' when ac.person_current_job_desc like '%Expert%' then 'expert'
							when ac.person_current_job_desc like '%Top Gun%' then 'topgun' when ac.person_current_job_desc like '%Associate%' then 'associate'
							when ac.person_current_job_desc like '%TmLdr%' then 'tmldr' when ac.person_current_job_desc like '%Supervisor%' then 'supervisor' 
							else 'other' end) as job,
							ac.person_inventory_total_hrs, 
							ac.person_mean_aph,
							ac.ml_aset_type, ac.shift_preference, ac.person_zip_code as person_zip
							from aiaes_data.ml_ipt_aset_classification ac
							where person_id in ({str_aset_avail}))  person_fields) dt1;
					"""


			df = self.redshift.get_sql_dataframe(query)
			return df


class RedshiftDBMS(object):
	'''
	Adds functionality for Redshift DBMS connection and command execution

	Services:
	Local variables and objects (self.):
		where: identifies last method for error handling
	'''

	def __init__(self):
		'''
		Initialize redshift connection manager
		'''

		self.where = ''
		self.lasttask = ''
		self.cnx_status = 'Not defined'
		self._set_redshift()

	def _set_redshift(self):
		'''Loads Redshift connection parameters'''
		self.where = "METHOD set_redshift"

		try:
			_envvar = 'DB_NAME'
			self.redshift = {'dbname': os.environ[_envvar]}

			_envvar = 'DB_HOST'
			self.redshift['host'] = os.environ[_envvar]

			_envvar = 'DB_PORT'
			self.redshift['port'] = os.environ[_envvar]
			_envvar = 'DB_USER'
			self.redshift['user'] = os.environ[_envvar]

			_envvar = 'DB_PWD'
			self.redshift['pwd'] = os.environ[_envvar]

			self.cnx_status = 'OK'
		except:
			self.redshift = None
			self.cnx_status = 'Environment Vars not found: ' + _envvar

	def _set_error(self, msg=''):
		_error_msg = str(sys.exc_info())

		if len(msg) > 0:
			_error_msg = msg + " " + _error_msg

		if len(self.lasttask) > 0:
			_error_msg += " - Last Task: " + self.lasttask
			self.lasttask = ''

		if len(self.where) > 0:
			_error_msg += " - Where: " + self.where
			self.where = ''

		return _error_msg

	def get_connect_string(self):
		return self.redshift['host']

	def open_conn(self):
		try:
			_con = psycopg2.connect(dbname=self.redshift['dbname'],
			                        host=self.redshift['host'],
			                        port=self.redshift['port'],
			                        user=self.redshift['user'],
			                        password=self.redshift['pwd'])

			return _con
		except Exception as ex:
			print(f"Connection String {self.get_connect_string()}")
			raise RuntimeError(self._set_error("Can't connect to Redshift " + self.get_connect_string()))

	def get_sql_dataframe(self, query, params=None):
		'''
		Connect to Redshift database and load panda dataframe with sql query results
		query: SQL command for data retrieve
		returns dataframe
		'''

		try:
			# print("Opening new Redshift connection")
			conn = self.open_conn()
			_result_df = pandas.read_sql(sql=query, params=params, con=conn)
			return _result_df
		except:
			raise RuntimeError(self._set_error("Can't execute SQL"))
		finally:
			if conn is not None and conn.closed == 0:
				# print("Close new Redshift connection")
				conn.close()
