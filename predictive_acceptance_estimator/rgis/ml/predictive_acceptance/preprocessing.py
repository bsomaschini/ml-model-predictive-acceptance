import pandas
import numpy
from sklearn.base import TransformerMixin
from imblearn.over_sampling import SMOTE
from time import time
from sklearn.preprocessing import OneHotEncoder	
import numpy as np
import pandas as pd

class Preprocessing:
    	
    def __init__(self,kwargs):
        self.ohe = OneHotEncoder(sparse=False,handle_unknown='ignore') #initialization for one hot encoding fit
        self.variable_list=[]                                          #initialization for name of encoded fields through one hot encoding
        self.encoding_rules={}                                         #initialization for likelihood encoding rules learning in the fit mode
 
    def mi_fix_values(self, X, criteria={}):
        data=X.copy()
        print("missing replacement with fix values")
        '''
        criteria: this is a disctionary where the key are the name of the columns and the 
                items are the values you would like to use to fill the NaNs in the specified
                column. 
        
        The criteria value is directly taken from the hyperparameters passed to the model, for futher explanation,
        please check the model technical documentation.
    '''
        start_time = time()
        data.fillna(value=criteria,inplace=True)
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        print("Missing values imputation with fix values completed. Time taken: {0}".format(time_taken))

        return data 
    
    def distance_encoding(self,X, fit_mode=False):
        '''
        Users can decide if considering the variable distance as numerical or encoding it through quartiles.
        
        NOTE: no dinstance encoding is applied, it could be done in future.
        '''
        X_copy=X.copy()
        start_time = time()
        print('Distance categorical_encoding...')
        if fit_mode:
            self.distance_quartiles=list(X_copy['distance_person_store_miles'].quantile([0,0.25,0.5,0.75,1]))
            X_copy['distance_person_store_miles']=pd.cut(x=X_copy['distance_person_store_miles'], bins=self.distance_quartiles,labels=["distance_high","distance_midhigh","distance_midlow","distance_low"]).astype('object')
            X_copy['distance_person_store_miles'].fillna("distance_missing",inplace=True)
        else:
            X_copy['distance_person_store_miles']=pd.cut(x=X_copy['distance_person_store_miles'], bins=self.distance_quartiles,labels=["distance_high","distance_midhigh","distance_midlow","distance_low"]).astype('object')
            X_copy['distance_person_store_miles'].fillna("distance_missing",inplace=True)
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        print("Distance encoding completed. Time taken: {0}".format(time_taken))
        return X_copy


	
    def one_hot_encoding(self,categorical_features:list, X, fit_mode=False): 
        '''this function allows to encode categorical variables creating a dummy variable for each category in each categorical
        feature.

        When the fit_mode is set to true the encoder learns the classes of each variable passed in the list: categorical_features.
        The encoders create the dummies stating if the observation present or not the i-th category.
        During the fit mode the name of the categories, which have been transformed in features is learned and saved in:
        self.variable_list.

        Whene fit_mode is False, then the pre-learned feature encoding is applyed to the inference categorical fields.
        If a categorical field in the new set presents a level not seen yet, this won't be considered. Moreover, if the 
        inference field doesn't present one or more levels which were in the training set field, then the dummy with the specific
        field is created but it takes always value 0.
        '''
        data=X.copy()
        start_time = time()
        print("One hot encoding...")
        #check if the user passed categorical features or not
        if len(categorical_features)>0:	
            if fit_mode:
                for n in categorical_features:
                    try:
                        #Save the name of the levels of the i-th categorical variable in a list.
                        self.variable_list=self.variable_list + [n+"_"+str(i) for i in np.unique(np.array(data[n])).tolist()]
                    except TypeError:
                        print("A variable contains mixed types: object and numbers, please replace values")
                self.ohe.fit(data[categorical_features])
            dtr=self.ohe.transform(data[categorical_features])
            data2=pd.DataFrame(dtr,columns=self.variable_list)  #creation of dummies
            data.reset_index(drop=True,inplace=True)
            data=data.join(data2)  #Join the dummy with the variable left in dataset
            time_taken = '%.2f ms' % ((time() - start_time) * 1000)
            print("One hot encoding completed. Time taken: {0}".format(time_taken))
            return data
        else:
            print("No catergorical feature to encode")
            return data
	
    def likelihood_encoding(self, X, categorical_features:list,targer_var,fit_mode=False):
        '''
        This function aims to substitue categories of qualitative or dummy features with a numerical value.
        Specifically this numerical value is the probability that the target class is equal to 1 (or Posisitve class)
        due to the fact that the explicative variable X is equal to class cl: P(Y=1|X=cl).
        This is calsulated a: (N. obs with Y=1 and X=cl)/(N. obs with X=cl)

        The operation is repeated for each variable in: categorical_features list.

        When fit_mode=True, the likelihood value is calculated on the trainingset, while, when fit_mode=False, the precalculated
        encoded value is applied to each class. Each new value is trated as unknown and it's coded with 0.
        '''
        data=X.copy()
        print("Likelihood encoding..")
        if len(categorical_features)>0:
            start_time = time()
            if fit_mode:                
                for vc in categorical_features:
                    table = pd.crosstab(index=data[targer_var], columns=data[vc])   #calculating the numer of occurences in target classes
                    encoding=dict(zip(table.columns.to_list(),table.values[1,]/np.sum(table.values,axis=0)))  
                    # Dictionary structure: category: likelihood  for each category of the i-th categorical variable.
                    self.encoding_rules[vc]=encoding  #the rules are saved for the future test set.
                    data.replace({vc:encoding},inplace=True) #replacement of values
            else:
                for key,item in self.encoding_rules.items():
                    #if the categories of the i-th categorical variable in the inference set are the same of the training set:
                    if len(list(set(np.unique(data[key]))-set(item.keys())))==0: 
                        #Replace with pre-trained replacement rule
                        data.replace({key:item},inplace=True)
                    else: 
                        #if new categories appear:
                        #Select new categories
                        new=list(set(np.unique(data[key]))-set(item.keys()))
                        #Add the new encoding rule: new_category=0
                        new_enc=dict(zip(new,[0]*len(new)))
                        item.update(new_enc)
                        data[key].replace(item,inplace=True)            
                        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
            print("likelihood encoding completed. Time taken: {0}".format(time_taken))
            return data	
        else:
            print("No catergorical feature to encode")
            return data	


    def target_extraction(self,data):
        ''' 
        Data for training (and the split validation set) are passed without target variable. This function allows the
        calculation of the target variable.

        Target: if (CHANGE_REQUEST_TYPE_CODE) or (NO_SHOW) are NOT null => 1
                else => 0
        '''
        start_time = time()
        try:
            #extract subset. Raise error if response variables are not in training data.
            Ys=data[["change_request_type_code","no_show"]]
        except KeyError:
            print("No target variables: change_request_type_code or no_show in training data")
        data["target_label"]=np.where(Ys["no_show"].isna(),np.where(Ys["change_request_type_code"].isna(),"no_change",Ys["change_request_type_code"]),np.where(Ys["change_request_type_code"].isna(),Ys["no_show"],Ys["no_show"]+"_"+Ys["change_request_type_code"]))
        data["target"]=np.where(data["target_label"]=="no_change",0,1)
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        print("Target variable cre completed. Time taken: {0}".format(time_taken))
        return data
	
    def data_prep(self,X,target,ohe=False,fit_mode=False,mi_fix_criteria={},categorical=[]):
        #Previous methods are applied sequentially
        data=X.copy()
        print("---Preprocessing---")
        if fit_mode:
            data=self.target_extraction(data)
        data=self.mi_fix_values(data,mi_fix_criteria)            
        #It is possible to choose if applying a One-hot-Encoding process or a Likelihood-Encoding process to encode
        #categorical features, stated in the list: categorical=[].
        # ohe=False -> likelihood_encoding
        # ohe=True -> one-hot-encoding
        if ohe:
            df=self.one_hot_encoding(X=data,categorical_features=categorical, fit_mode=fit_mode)
        else:
            df=self.likelihood_encoding(X=data,categorical_features=categorical,targer_var=target,fit_mode=fit_mode)
        return df


class UnderSampling(TransformerMixin):
    def __init__(self, max_sample_size):
        self.max_sample_size = max_sample_size

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, y=None, **kwargs):
        start_time = time()
        down_samples = []
        X_copy = X.copy()
        for req_type_code, req_type_df in X_copy.groupby(['CHANGE_REQUEST_TYPE_CODE']):
            sample_size = self.max_sample_size if req_type_df.shape[0] > self.max_sample_size else req_type_df.shape[0]
            down_samples.append(req_type_df.sample(n=sample_size, random_state=99))

        output = pandas.concat(down_samples).sample(frac=1)  # shuffles up the rows
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        print("Under sampling of No Change observations completed. Time taken: {0}".format(time_taken))
        return output


class SMOTEOverSampling(TransformerMixin):
    def __init__(self, features, target):
        self.features = features
        self.target = target

    def fit(self, X, y=None, **fit_params):
        return self

    def transform(self, X, y=None, **kwargs):
        start_time = time()
        X_copy = X.copy()
        print("Data shape before SMOTE: {0}".format(X_copy.shape))
        X_resampled, y_resampled = SMOTE().fit_resample(
            X=X_copy[self.features], y=X_copy[self.target])

        df_resampled = pandas.DataFrame(X_resampled)

        df_resampled.columns = self.features
        df_resampled[self.target] = y_resampled
        df_resampled = df_resampled.infer_objects()

        print("Data shape after SMOTE: {0}".format(df_resampled.shape))
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        #print("Resampled count: {0}".format(Counter(df_resampled[self.target])))
        print("Resampling of least observed change request types completed. Time taken: {0}".format(time_taken))
        return df_resampled
