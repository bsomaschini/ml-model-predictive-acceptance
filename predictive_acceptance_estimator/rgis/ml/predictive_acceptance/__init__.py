from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, confusion_matrix, classification_report,roc_curve, auc
import xgboost as xgb
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold, cross_val_score
import warnings
from time import time
from numpy import argwhere, std
from scipy.stats import skew
import pandas as pd
import numpy as np
import json
import yaml
import matplotlib.pyplot as plt

from .preprocessing import *

model_file_name = "rgis-predictive-acceptance-estimator.pkl"
random_seed = 999

features="per_aph_avg,tenure,age,person_inventory_total_hrs,distance_person_store_miles,is_backroom_flag,is_night_shift_flag,is_weekend_flag,is_rx_event_flag" 
categorical_columns="job,ml_aset_type,shift_preference"
fix_mi_variable={'per_aph_avg':2135.12,'is_backroom_flag':2, 'is_msr_flag':2,'is_night_shift_flag':2,'is_weekend_flag':2,'is_rx_event_flag':2,'distance_person_store_miles':26.13,'age':38,'tenure':4.83,'job':'unknown_job','person_inventory_total_hrs':0,'ml_aset_type':'-1','shift_preference':2}


class PredictiveAcceptanceEstimator(TransformerMixin,Preprocessing):
    """
    Class defines the pipeline for predictive acceptance model
    """
    def __init__(self, **kwargs):
        self.ohe = OneHotEncoder(sparse=False,handle_unknown='ignore')  #initialization for one hot encoding fit (from preprocessing)
        self.variable_list=[]                                           #initialization for name of encoded fields through one hot encoding (from preprocessing)
        self.encoding_rules={}                                          #initialization for likelihood encoding rules learning in the fit mode (from preprocessing)
        self.__features=features.split(',')               #features in model
        self.__categoricalFeatures=categorical_columns.split(',')     #categorical features in dataset
        self.fix_missing=fix_mi_variable       #Missing imputation rules with fix values for stated features.
        self.rf_params = {
            'bootstrap':kwargs.get("bootstrap"),
            'max_depth':int(float(kwargs.get("max_depth"))),
            'max_features':int(float(kwargs.get("max_features"))),
            'min_samples_leaf':int(float(kwargs.get("min_samples_leaf"))),
            'min_samples_split':int(float(kwargs.get("min_samples_split"))),
            'n_estimators':int(float(kwargs.get("n_estimators"))),
            }
        self.model=RandomForestClassifier(**self.rf_params)

               

    def fit(self, X, y=0, ohe_mode=True, **kwargs):
        warnings.filterwarnings('ignore')        
        print("---Fit---")
        #Start preprocessing in fit mode.
        X_preprocessed=self.data_prep(X,target='target',ohe=ohe_mode,fit_mode=True,mi_fix_criteria=self.fix_missing, categorical=self.__categoricalFeatures)
        # The name of features, stating the features to use in the model, must be integrated with:
        #   self.__categoricalFeatures if ohe=False
        #   self.variable_list if ohe=True
        if ohe_mode:
            self.__features=self.__features + self.variable_list
        else:
            self.__features=self.__features + self.__categoricalFeatures
        start_time = time()
        #Resampling of the observations: method=SMOTE
        #As result of the SMOTE process a resampled dataset with only the selected fetures in the model
        #is returned.
        data_balanced=SMOTEOverSampling(features=self.__features,target="target").transform(X_preprocessed)
        #Model fit:
        self.model.fit(data_balanced[self.__features], data_balanced['target'])
        time_taken = '%.2f ms' % ((time() - start_time) * 1000)
        print("Predictive Acceptance model fit has been completed. Time taken: {0}".format(time_taken))

        return self
    
    def predict(self,X, ohe_mode=True,is_complex_index=False, validation=False):
        warnings.filterwarnings('ignore') 
        if  X.empty:
            raise ValueError ("No inference data available, prediction aborted.")    
        else:
            print("---Predict---")
            #Preprocessing
            X_preprocessed=self.data_prep(X,target='target',ohe=ohe_mode,fit_mode=False,mi_fix_criteria=self.fix_missing, categorical=self.__categoricalFeatures)
            start_time = time()
            #This flag must be considered only when the validation procedure must be used.
            #It allows to return a proper dataset with the prediction in probability and in classes appended.
            if validation==True:
                X["predict_proba"]=self.model.predict_proba(X_preprocessed[self.__features])[:,0]
                X["prediction"]=self.model.predict(X_preprocessed[self.__features])
                time_taken = '%.2f ms' % ((time() - start_time) * 1000)
                print("Predictive Acceptance model predict has been completed. Time taken: {0}".format(time_taken))
                return X
            else:
                if is_complex_index and 'solution_id' in X.columns:
                    results_ci={}
                    for index_ev,ev_subset in X_preprocessed.groupby(["scheduled_event_id"]):
                        sol_dict={}
                        for index_sol,sol_subset in ev_subset.groupby(["solution_id"]):
                            #For each Event, for each possible solution the mean acceptance score is provided
                            #This represent the average acceptance score of the entire team.
                            y_prob=self.model.predict_proba(sol_subset[self.__features])[:,0]
                            acceptance_score=np.mean(y_prob)
                            sol_dict[index_sol]={"acceptance_score":acceptance_score,"aes_team":np.unique(sol_subset["person_id"]).tolist()}
                        results_ci[index_ev]=sol_dict
                    time_taken = '%.2f ms' % ((time() - start_time) * 1000)
                    print("Predictive Acceptance model predict has been completed. Time taken: {0}".format(time_taken))
                    return results_ci
                else:
                    #If not complex index mode, the results for SEA are provided in a nested dictionary where
                    #the primary key is the event and the value is a dictionary where the keys is the person_id of each team
                    #memeber and the value is the acceptance score probability.
                    results_sea={}
                    for index,subset in X_preprocessed.groupby(["scheduled_event_id"]):
                        y_prob=self.model.predict_proba(subset[self.__features])[:,0]
                        acceptance_score_dict=dict(zip(subset["person_id"].tolist(),y_prob))
                        results_sea[index]=acceptance_score_dict
                    time_taken = '%.2f ms' % ((time() - start_time) * 1000)
                    print("Predictive Acceptance model predict has been completed. Time taken: {0}".format(time_taken))
                    return results_sea
        
    def results_validation(self,X, ohe_mode=True):
        warnings.filterwarnings('ignore') 
        X_copy=X.copy()
        pred=self.predict(X_copy, ohe_mode=ohe_mode,validation=True)
        X_copy=pred
        X_copy=self.target_extraction(X_copy)
        cm=confusion_matrix(X_copy["target"],X_copy["prediction"])
        print(classification_report(X_copy["target"], X_copy["prediction"], target_names=["accepted","rejected"]))
        fpr, tpr= roc_curve(X_copy["target"], X_copy["prediction"], pos_label=1)
        auc_value=auc(fpr,tpr)
        print(auc_value)
        plt.figure()
        lw = 2
        plt.plot(fpr, tpr, color='darkorange',lw=lw, label='ROC curve (area = %0.2f)' % auc_value)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")
        plt.show()
        return (X_copy,cm,auc_value)


if __name__ == '__main__': 
    pass




