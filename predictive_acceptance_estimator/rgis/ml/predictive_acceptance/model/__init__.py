import dill
import os
from time import time

from rgis.ml.predictive_acceptance import model_file_name

base_dir = os.path.dirname(__file__)

model_instance = None


def predict(X, reload_model=False, **kwargs):
	"""
	this method loads the model from the same directory and predicts the output
	:param X: inference features
	:param reload_model: reload the pickle when True
	:param kwargs: verbose - prints description for the acceptance score when True
	:return: prediction outcomes
	"""
	verbose = kwargs.get("verbose", True)
	start_time = time()
	global model_instance
	reloaded = False
	if model_instance is None or reload_model:
		model_instance = load_model()
		reloaded = True

	prediction = model_instance.predict(X, verbose=verbose)
	time_taken = '%.2f ms' % ((time() - start_time) * 1000)
	print("Model (re)loaded: {0} and Time taken for prediction: {1}".format(reloaded, time_taken))
	return prediction


def load_model():
	"""
	this function loads model from pickle file
	:return:
	"""
	model_path = os.path.join(base_dir, model_file_name)

	with open(model_path, 'rb') as f:
		return dill.load(f)
