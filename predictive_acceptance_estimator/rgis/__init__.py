import os
from subprocess import Popen, PIPE


def build_model_package(base_dir, output_dir):

	package = os.path.join(base_dir, "dist/rgis-0.1.tar.gz")
	print("Package path: {0}".format(package))

	commands = [["ls -ltr"],
                ["chmod +x rgis"],
                ["python setup.py sdist"],
                ["cp -avr {0} {1}".format(package, output_dir)]]
    
	for cmd in commands:
		print("Executing command {0}".format(" ".join(cmd)))
		p = Popen(cmd, shell=True)
		print(p.communicate())
