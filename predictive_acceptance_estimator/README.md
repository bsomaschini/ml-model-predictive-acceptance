This package contains dependencies of Predictive Acceptance model.

Module Structure:

    rgis
        - ml
            - predictive_acceptance
                - PredictiveAcceptanceEstimator
                - preprocessing
                    - ConvertToDateTime
                    - RemoveNanValues
                    - FillNaValues
                    - PickContextEvents
                    - AggregatePersonData
                    - LabelEncoding
                    - BooleanEncoding
                    - UnderSampling
                    - SMOTEOverSampling