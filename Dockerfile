# Build an image that can do training and inference in SageMaker
# This is a Python 2 image that uses the nginx, gunicorn, flask stack
# for serving inferences in a stable way.

FROM continuumio/miniconda3

RUN mkdir -p /opt/ml/output

RUN apt-get -y update && apt-get install -y --no-install-recommends \
         wget \
         nginx \
         ca-certificates \
         libgomp1 \
    && rm -rf /var/lib/apt/lists/*
    

# Here we get all python packages.
# There's substantial overlap between scipy and numpy that we eliminate by
# linking them together. Likewise, pip leaves the install caches populated which uses
# a significant amount of space. These optimizations save a fair amount of space in the
# image, which reduces start up time.

CMD [ "/bin/bash", "source activate base" ]

RUN pip install numpy==1.16.2 scipy==1.2.1 scikit-learn==0.20.2 pandas==0.24.0 flask gevent gunicorn xgboost==0.82 imbalanced-learn==0.4.3 dill==0.2.9 pyYaml seaborn boto3 datetime psycopg2-binary  joblib==0.13.2

# Set some environment variables. PYTHONUNBUFFERED keeps Python from buffering our standard
# output stream  which means that logs can be delivered to the user quickly. PYTHONDONTWRITEBYTECODE
# keeps Python from writing the .pyc files which are unnecessary in this case. We also update
# PATH so that the train and serve programs are found when the container is invoked.

ENV PYTHONUNBUFFERED=TRUE
ENV PYTHONDONTWRITEBYTECODE=TRUE
ENV PATH="/opt/program:${PATH}"

RUN mkdir -p /opt/ml/output
RUN mkdir -p /opt/ml/input

# Set up the program in the image
COPY predictive_acceptance_estimator /opt/program

RUN chmod +x /opt/program/train /opt/program/serve

WORKDIR /opt/program
